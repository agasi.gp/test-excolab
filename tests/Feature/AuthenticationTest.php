<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function user_can_login_with_correct_credential(): void
    {
        $user = User::create([
            'name' => 'Agasi Gilang Persada',
            'email' => 'agasigp@live.com',
            'password' => 'agasigp@live.com',
        ]);
        $response = $this->post('/api/sanctum/token', [
            'email' => 'agasigp@live.com',
            'password' => 'agasigp@live.com',
            'device_name' => 'Android',
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'OK',
            ]);
    }

    /**
     * @test
     */
    public function user_can_logout(): void
    {
        $user = User::create([
            'name' => 'Agasi Gilang Persada',
            'email' => 'agasigp@live.com',
            'password' => 'agasigp@live.com',
        ]);
        $this->post('/api/sanctum/token', [
            'email' => 'agasigp@live.com',
            'password' => 'agasigp@live.com',
            'device_name' => 'Android',
        ]);

        $response = $this->post('/logout');
        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'OK',
                'data' => [
                    'token' => $user->tokens[0]->token
                ]
            ]);
    }
}
